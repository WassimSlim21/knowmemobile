/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Sabaa
 */
public class User {
    private int id;
    private String email;
    private String f_name;
    private String l_name;
    private String pwd;
    private String location;
    private int age;
    private String image;
    private String roles;

    public User(int id, String email, String f_name, String l_name, String pwd, String location, int age, String image, String roles) {
        this.id = id;
        this.email = email;
        this.f_name = f_name;
        this.l_name = l_name;
        this.pwd = pwd;
        this.location = location;
        this.age = age;
        this.image = image;
        this.roles = roles;
    }
    

    public User(String f_name, String l_name, String email, String pwd, int age) {
        this.f_name = f_name;
        this.l_name = l_name;
        this.email = email;
        this.pwd = pwd;
        this.age = age;
    }

    public User() {
        
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFname() {
        return f_name;
    }

    public void setFname(String f_name) {
        this.f_name = f_name;
    }

    public String getLname() {
        return l_name;
    }

    public void setLname(String l_name) {
        this.l_name = l_name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", age=" + age + ", f_name=" + f_name + ", l_name=" + l_name + ", pwd=" + pwd + ", location=" + location + ", image=" + image + ", roles=" + roles + ", email=" + email + '}';
    }
    
}
