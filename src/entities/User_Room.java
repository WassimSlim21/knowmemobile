/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author wassi
 */
public class User_Room {
    private int id;
    private int room_id;
    private int user_id;
    private Date date;

    public int getId() {
        return id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    

    public User_Room(int id, int room_id, int user_id, Date date) {
        this.id = id;
        this.room_id = room_id;
        this.user_id = user_id;
        this.date = date;
    }
    
        public User_Room() {
        super();
    }

    @Override
    public String toString() {
        return "User_Room{" + "id=" + id + ", room_id=" + room_id + ", user_id=" + user_id + ", date=" + date + '}';
    }

 
    
    
}
