package entities;


public class Room {
	
	private int id;
	private Menu menu;
	private String name;
	private String description;
	private String location;
        private String image;

      


        
	public Room(int id, Menu menu, String name, String description, String location) {
		super();
		this.id = id;
		this.menu = menu;
		this.name = name;
		this.description = description;
		this.location = location;

	}

    public Room() {
    }

    public Room(int id, Menu menu, String name, String description, String location, String image) {
        this.id = id;
        this.menu = menu;
        this.name = name;
        this.description = description;
        this.location = location;
        this.image = image;

    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", Menu=" + menu + ", name=" + name + ", description=" + description + ", location=" + location + ", image=" + image + '}';
    }

    
	
	
	
	

}
