package entities;

import java.util.Date;

public class Match {
	private int id;
	private int user1;
	private int user2;

	private String date;

	
	
	public Match(int id, int user1, int user2, String date) {
		super();
		this.id = id;
		this.user1 = user1;
		this.user2 = user2;

		this.date = date;

	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getUser1() {
		return user1;
	}



	public void setUser1(int user1) {
		this.user1 = user1;
	}



	public int getUser2() {
		return user2;
	}



	public void setUser2(int user2) {
		this.user2 = user2;
	}


	public Match() {
		super();
	}



	@Override
	public String toString() {
		return "Match [id=" + id + ", user1=" + user1 + ", user2=" + user2 + ", date=" + date + "]";
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}

	
	
	

}
