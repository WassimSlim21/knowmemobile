/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author msi
 */
public class Event {

    private int id;
    private String name;
    private String description;
    private EventCategory category;
    private Date date;
    private String image;
    private int NbrParticipant;
    private int NbrJoined;

    public EventCategory getCategory() {
        return category;
    }

    public Event(String name, String description, String image, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.NbrParticipant = NbrParticipant;
    }

    public Event(String name, String description, Date date, String image, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.image = image;
        this.NbrParticipant = NbrParticipant;
    }

    public Event(String name, String description, EventCategory category, Date date, String image, int NbrParticipant, int NbrJoined) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.date = date;
        this.image = image;
        this.NbrParticipant = NbrParticipant;
        this.NbrJoined = NbrJoined;
    }

    public void setCategory(EventCategory category) {
        this.category = category;
    }

    public Event(int id) {
        this.id = id;

    }

    public Event(String name) {
        this.name = name;
    }

    public Event(String name, String description, Date date, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.NbrParticipant = NbrParticipant;

    }

    public Event(int id, String name, String description, Date date, int NbrParticipant) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.NbrParticipant = NbrParticipant;
    }

    public Event(String name, String description, EventCategory category, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.NbrParticipant = NbrParticipant;

    }

    public Event(String name, String description, EventCategory category, Date date, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.date = date;
        this.NbrParticipant = NbrParticipant;

    }

    public Event(int id, String name, String description, Date date, String image, int NbrParticipant, int NbrJoined) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.image = image;
        this.NbrParticipant = NbrParticipant;
        this.NbrJoined = NbrJoined;

    }

    public Event(String name, String description, int NbrParticipant) {
        this.name = name;
        this.description = description;
        this.NbrParticipant = NbrParticipant;

    }

    public Event() {

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNbrParticipant() {
        return NbrParticipant;
    }

    public void setNbrParticipant(int NbrParticipant) {
        this.NbrParticipant = NbrParticipant;
    }

    public int getNbrJoined() {
        return NbrJoined;
    }

    public void setNbrJoined(int NbrJoined) {
        this.NbrJoined = NbrJoined;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event{" + "id=" + id + ", name=" + name + ", description=" + description + ", image=" + image + ", NbrParticipant=" + NbrParticipant + ", NbrJoined=" + NbrJoined + ", date=" + date + '}';
    }

}
