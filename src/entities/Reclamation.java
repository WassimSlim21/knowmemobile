/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Mehdi
 */
public class Reclamation {
    private int id;
    private String objet;
    private String content;
    private String type;
    private int SentBy ;
    private String createdAt;

    public Reclamation() {
    }

    public Reclamation(int id, String objet, String content, String type, int SentBy, String createdAt) {
        this.id = id;
        this.objet = objet;
        this.content = content;
        this.type = type;
        this.SentBy = SentBy;
        this.createdAt = createdAt;
    }

    public Reclamation(String objet, String content, String type, String createdAt) {
        this.objet = objet;
        this.content = content;
        this.type = type;
        this.createdAt = createdAt;
    }

    public Reclamation(String objet, String content, String type, int SentBy, String createdAt) {
        this.objet = objet;
        this.content = content;
        this.type = type;
        this.SentBy = SentBy;
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSentBy() {
        return SentBy;
    }

    public void setSentBy(int SentBy) {
        this.SentBy = SentBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    
    
    
  


    
}
