package entities;

import java.util.Date;

public class Like {
	
	private int id;
	private int user_id_1;
	private int user_id_2;
	private Date date;
	private Boolean like;
		
	public Like() {
		super();
	}
	
	public Like(int id, int user_id_1, int user_id_2, Date date, Boolean like) {
		super();
		this.id = id;
		this.user_id_1 = user_id_1;
		this.user_id_2 = user_id_2;
		this.date = date;
		this.like = like;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id_1() {
		return user_id_1;
	}
	public void setUser_id_1(int user_id_1) {
		this.user_id_1 = user_id_1;
	}
	public int getUser_id_2() {
		return user_id_2;
	}
	public void setUser_id_2(int user_id_2) {
		this.user_id_2 = user_id_2;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Boolean getLike() {
		return like;
	}
	public void setLike(Boolean like) {
		this.like = like;
	}
	
	
	@Override
	public String toString() {
		return "Like [id=" + id + ", user_id_1=" + user_id_1 + ", user_id_2=" + user_id_2 + ", date=" + date + ", like="
				+ like + "]";
	}
	
	
	

}
