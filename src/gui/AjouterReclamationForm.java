/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.codename1.components.InfiniteProgress;

import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.Component;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;

import java.util.Date;
import entities.Reclamation;
import services.ServiceReclamation;
import services.ServiceReclamation;

/**
 *
 * @author Mehdi
 */
public class AjouterReclamationForm extends BaseForm {
    Form current;
    public AjouterReclamationForm(Resources res){
    super("Ajouter Reclamation",BoxLayout.y());
    Toolbar tb = new Toolbar(true);
    current = this;
    setToolbar(tb);
    getTitleArea().setUIID("container");
    getContentPane().setScrollVisible(false);
    
    TextField Objet = new TextField("","Objet");
    Objet.setUIID("TextFieldBlack");
    addStringValue("Objet",Objet);
    TextArea content = new TextField("","Content...");
    content.setUIID("TextFieldBlack");
    addStringValue("content",content);
    Button btnAjouter = new Button("Valider");
    addStringValue("",btnAjouter);
    btnAjouter.addActionListener((e)->{
        try{
            if(Objet.getText()=="" || content.getText()==""){
                Dialog.show("Verify your data.","","Annuler","OK");
            }
            else {
                InfiniteProgress ip = new InfiniteProgress(); 
                final Dialog iDialog = ip.showInfiniteBlocking();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                
                Reclamation r = new Reclamation(
                        String.valueOf(Objet.getText()).toString(),
                        String.valueOf(content.getText()).toString(),
                        "Abus",
                        format.format(new Date())
                );
                System.out.print("data="+ r);
                
                ServiceReclamation.getInstance().save(r);
                iDialog.dispose();
                refreshTheme();
                        
                        
                        
                        
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    });
    
            }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s,"PaddedLabel"))
        .add(BorderLayout.CENTER,v));
        add(createLineSeparator(0xeeeeee));
        
    }
}
