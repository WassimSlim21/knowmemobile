/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.codename1.ui.Button;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author msi
 */
public class HomeForm extends Form {

    Form current;
    Resources res;

    public HomeForm() {
        super("Home", BoxLayout.y());
        //setTitle("Home");
        //setLayout(BoxLayout.y());
        current = this; //Récupération de l'interface(Form) en cours

        add(new Label("Choose an option"));
        Button btnListEvents = new Button("List events");
        Button btnAddEvent = new Button("Add event");
        Button btnListRoom = new Button("List Room");
        Button btnAjouterRoom = new Button("Ajouter Room");
        btnListEvents.addActionListener(e -> new listEvents(current).show());
        btnAddEvent.addActionListener(e -> new addEvent(current).show());
        btnListRoom.addActionListener(e -> new listRooms(current).show());
        btnAjouterRoom.addActionListener(e -> new AjouterRoom(current).show());
        add(new Label("Choose an option"));
        Button btnAddTask = new Button("Add user");
        Button btnListTasks = new Button("List users");

        btnAddTask.addActionListener(e -> new AddUserForm(res).show());
        btnListTasks.addActionListener(e -> new ListUserForm(res).show());
        current.addAll(btnAddTask, btnListTasks);

        current.addAll(btnListEvents,btnAddEvent,btnListRoom,btnAjouterRoom);

    }
}
