/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import entities.User;
import services.UserService;

/**
 *
 * @author Sabaa
 */
public class AddUserForm extends Form{
    
    Form current;
    public AddUserForm(Resources res){
        super("Add a new user", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        current = this;
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        //setTitle("Add a new user");
        getContentPane().setScrollVisible(false);
        
        tb.addSearchCommand(e -> {
            
        });
        
        Tabs swipe = new Tabs();
        
        Label spacer1 = new Label();
        Label spacer2 = new Label();
        
       // addTab(swipe, spacer1, res.getImage("logo.png"),"","",res);
        
        //
          swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }
if(rbs[0] != null){
        rbs[0].setSelected(true);
    
}
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton mesListes = RadioButton.createToggle("Mes Reclamations", barGroup);
        mesListes.setUIID("SelectBar");
        RadioButton liste = RadioButton.createToggle("Autres", barGroup);
        liste.setUIID("SelectBar");
        RadioButton partage = RadioButton.createToggle("Ajouter Utilisateur", barGroup);
        partage.setUIID("SelectBar");
        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");


        mesListes.addActionListener((e) -> {
               InfiniteProgress ip = new InfiniteProgress();
        final Dialog ipDlg = ip.showInifiniteBlocking();
        
        //  ListReclamationForm a = new ListReclamationForm(res);
          //  a.show();
            refreshTheme();
        });

        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(3, mesListes, liste, partage),
                FlowLayout.encloseBottom(arrow)
        ));

        partage.setSelected(true);
        arrow.setVisible(false);
        addShowListener(e -> {
            arrow.setVisible(true);
            updateArrowPosition(partage, arrow);
        });
        bindButtonSelection(mesListes, arrow);
        bindButtonSelection(liste, arrow);
        bindButtonSelection(partage, arrow);
        // special case for rotation
        addOrientationListener(e -> {
            updateArrowPosition(barGroup.getRadioButton(barGroup.getSelectedIndex()), arrow);
        });
        
        //
        
        TextField tfName = new TextField("","Nom de l'utilisateur");
        tfName.setUIID("TextFieldBlack");
        addStringValue("Nom", tfName);
        
        TextField tfPrenom = new TextField("","Prenom de l'utilisateur");
        tfPrenom.setUIID("TextFieldBlack");
        addStringValue("Prenom", tfPrenom);
        
        TextField tfEmail = new TextField("","Email de l'utilisateur");
        tfEmail.setUIID("TextFieldBlack");
        addStringValue("Email", tfEmail);
        
        TextField tfPwd = new TextField("","Mot de passe");
        tfPwd.setUIID("TextFieldBlack");
        addStringValue("Mot de passe", tfPwd);
        
        TextField tfAge = new TextField("","Age de l'utilisateur");
        tfAge.setUIID("TextFieldBlack");
        addStringValue("Age", tfAge);
        
        
        Button btnValider = new Button("Ajouter");
        addStringValue("", btnValider);
        
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if ((tfName.getText().length()==0)||(tfPrenom.getText().length()==0)||(tfAge.getText().length()==0) ||(tfEmail.getText().length()==0)||(tfPwd.getText().length()==0) )
                        Dialog.show("Alert", "Please fill all the fields", "Annuler","OK");
                    else{
                        InfiniteProgress ip = new InfiniteProgress();
                        final Dialog iDialog = ip.showInfiniteBlocking();
                        
                        
                        User u = new User(String.valueOf(tfName.getText()).toString(),
                                String.valueOf(tfPrenom.getText()).toString(),
                                String.valueOf(tfEmail.getText()).toString(),
                                String.valueOf(tfPwd.getText()).toString(),
                                Integer.parseInt(tfAge.getText())
                        );
                        
                        System.out.println("data user == "+u);
                        
                        new UserService().addUser(u);
                        
                       
                        iDialog.dispose(); //bech na7i dialog baed ma3maltha
                        
                        new ListUserForm(res).show();
                        refreshTheme();
                    }
                }catch(Exception ex ){
                    ex.printStackTrace();
                }
            }
        });
        //setTitle("Add a new user");
        //setLayout(BoxLayout.y());
        
        //TextField tfName = new TextField("","Nom de l'utilisateur");
        //TextField tfPrenom = new TextField("","Prenom de l'utilisateur");
        //TextField tfAge= new TextField("", "age");
        //Button btnValider = new Button("Add user");
        
        //btnValider.addActionListener(new ActionListener() {
        //    @Override
        //    public void actionPerformed(ActionEvent evt) {
       //         if ((tfName.getText().length()==0)||(tfPrenom.getText().length()==0)||(tfAge.getText().length()==0))
        //            Dialog.show("Alert", "Please fill all the fields", new Command("OK"));
        //        else
        //        {
        //            try {
        //                User u = new User(tfName.getText(), tfPrenom.getText(),Integer.parseInt(tfAge.getText()));
        //                if( new UserService().addUser(u))
         //                   Dialog.show("Success","Connection accepted",new Command("OK"));
        //                else
        //                    Dialog.show("ERROR", "Server error", new Command("OK"));
        //            } catch (NumberFormatException e) {
        //                Dialog.show("ERROR", "Status must be a number", new Command("OK"));
        //            }   
        //        }
           // }
        //});
        //this.addAll(tfName,tfPrenom,tfAge,btnValider);
        //this.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_ARROW_BACK, e-> previous.showBack()); // Revenir vers l'interface précédente
    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.east(new Label(s, "PaddedLabel"))
        .add(BorderLayout.CENTER, v));
        //add(createLineSeparator(0xeeeeee));
    }

    private void addTab(Tabs swipe,Label spacer, Image image, String string, String text, Resources res) {
        int size= Math.min(Display.getInstance().getDisplayWidth(),Display.getInstance().getDisplayHeight());
        
        if(image.getHeight() < size){
            image = image.scaledHeight(size);
        }
        
        if(image.getHeight() > Display.getInstance().getDisplayHeight() / 2){
             image = image.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
        }
        
        ScaleImageLabel imageScale =new ScaleImageLabel(image);
        imageScale.setUIID("Container");
        imageScale.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        
        Label overLay = new Label("", "ImageOverlay");
        
        Container page1 = 
                LayeredLayout.encloseIn(
                imageScale,
                        overLay,
                        BorderLayout.south(
                        BoxLayout.encloseY(
                                new SpanLabel(text, "LargeWhiteText"),
                                        spacer
                        )
                        )
                );
        
        swipe.addTab("", res.getImage("logo.png"), page1);
                
    }
    
    public void bindButtonSelection(Button btn, Label l){
        btn.addActionListener(e-> {
            if(btn.isSelected())
                updateArrowPosition(btn,l);
        });
    }

    private void updateArrowPosition(Button btn, Label l) {
        l.getUnselectedStyle().setMargin(LEFT, btn.getX() + btn.getWidth() / 2 -l.getWidth() / 2);
        l.getParent().repaint();
    } 
}
