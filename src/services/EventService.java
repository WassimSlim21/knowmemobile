/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import utils.DataSource;
import utils.Statics;
import java.util.Map;
import entities.Event;
import entities.EventCategory;


/**
 *
 * @author msi
 */
public class EventService {

    public static EventService instance;
    private ArrayList<Event> events;
    public boolean resultOK;
    private ConnectionRequest request;

    public EventService() {
        this.events = new ArrayList<Event>();
        request = new ConnectionRequest();
    }

    public static EventService getInstance() {
        if (instance == null) {
            instance = new EventService();
        }
        return instance;
    }

    public boolean addEvent(Event e) {
        Date aujourdhui = new Date();
        String tt = "";
        String url = Statics.BASE_URL + "/AddEvent?name=" + e.getName() + "&description=" + e.getDescription()
                + "&image=" + e.getImage() + "&NbrParticipant=" + e.getNbrParticipant();
        request.setUrl(url);// Insertion de l'URL de notre demande de connexion
        request.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = request.getResponseCode() == 200; //Code HTTP 200 OK   
                request.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(request);
        return resultOK;
    }

    public ArrayList<Event> parseEvent(String jsonText) {
        try {
            JSONParser j = new JSONParser();// Instanciation d'un objet JSONParser permettant le parsing du résultat json
            Map<String, Object> eventsListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) eventsListJson.get("root");
            System.out.println("list aaa " + list);
            for (Map<String, Object> obj : list) {
                Event eve = new Event();
                float id = Float.parseFloat(obj.get("id").toString());
                float nbr = Float.parseFloat(obj.get("NbrParticipant").toString());
                eve.setId((int) id);
                eve.setName(obj.get("name").toString());
                eve.setDescription(obj.get("description").toString());
                eve.setImage(obj.get("image").toString());
                eve.setNbrParticipant((int) nbr);
                events.add(eve);

            }

        } catch (IOException ex) {
            ex.getStackTrace();
        }
        return events;
    }

    public ArrayList<Event> getAllEvents() {
        String url = Statics.BASE_URL + "/AfficherEvent";
        request.setUrl(url);
        request.setPost(false);

        request.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                events = parseEvent(new String(request.getResponseData()));
                request.removeResponseListener(this);
            }
        });
        System.out.println("Network Manager : " + NetworkManager.getInstance());
        NetworkManager.getInstance().addToQueueAndWait(request);
        System.err.println("request te3i dsqdqsds:" + events);

        return events;
    }
}
