/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;

import com.codename1.processing.Result;

import com.codename1.ui.events.ActionListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import entities.Reclamation;
import entities.User;
import utils.Statics;

/**
 *
 * @author Mehdi
 */
public class ServiceReclamation {
    public static ServiceReclamation instance = null;
       private boolean resultOK;

    private ConnectionRequest req;
    public static ServiceReclamation getInstance(){
        if (instance == null)
            instance = new ServiceReclamation();
        return instance;
    }
    public ServiceReclamation(){
        req = new ConnectionRequest();
    }
    public void ajouterReclamation(Reclamation r){
        
        String URL = Statics.BASE_URL+"/addReclamation?Objet="+r.getObjet()+"&content="+r.getContent()+"&type="+r.getType();
        req.setUrl(URL);
        req.addResponseListener((e)->{
            String str =new String(req.getResponseData());
            System.out.print("data="+str);
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        
    }
    
    public ArrayList<Reclamation>afficherReclamation(){
        ArrayList<Reclamation> reclamations = new ArrayList<>();
        req.setUrl(Statics.BASE_URL+"/Reclamation");
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                JSONParser Json;
                Json = new JSONParser();
                try {
                    Map<String,Object>mapReclamations = Json.parseJSON(new CharArrayReader(new String(req.getResponseData()).toCharArray()));
                    List<Map<String,Object>> listOfMaps = (List<Map<String,Object>>) mapReclamations.get("root");
                    for (Map<String,Object> obj : listOfMaps){
                        Reclamation r = new Reclamation();
                        float id =Float.parseFloat(obj.get("id").toString());
                        String Objet = obj.get("Objet").toString();
                        String content = obj.get("content").toString();
                        String type = obj.get("type").toString();
                        String DateConverter = obj.get("createdAt").toString().substring(obj.get("createdAt").toString().indexOf("timestamp")+10 , obj.get("obj").toString().lastIndexOf(")"));
                        Date currentDate = new Date (Double.valueOf(DateConverter).longValue()*1000);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        String dateString = formatter.format(currentDate);
                        r.setId((int)id);
                        r.setObjet(Objet);
                        r.setContent(content);
                        r.setType(type);
                        r.setCreatedAt(dateString);
                        
                        reclamations.add(r);
                    
                        
                       
                    }
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
                
            
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return reclamations;
    }
    
     public boolean save(Reclamation reclamation) {
        String url = Statics.BASE_URL + "/addReclamation";
        req.setUrl(url);
        req.setPost(true);
        req.setContentType("application/json");
        try {
            System.out.println("hani fi wist lajout");
            HashMap<String, Object> hashMap = new HashMap<>();
            User connectedUser = new User(2, "test", "test", "test", "test", "test", 0, "test", "test");
            reclamation.setSentBy(connectedUser.getId());
            hashMap.put("user", reclamation.getSentBy());
            hashMap.put("Objet", reclamation.getObjet());
            hashMap.put("content", reclamation.getContent());
            hashMap.put("type", reclamation.getType());
            System.out.println(hashMap);
            req.setRequestBody(Result.fromContent(hashMap).toString());
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                    req.removeResponseListener(this);
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            return resultOK;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
}
}
