/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import entities.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import utils.DataSource;
import utils.Statics;

/**
 *
 * @author Sabaa
 */
public class UserService {
    
    public ArrayList<User> users;
    public boolean resultOK;
    private ConnectionRequest request;

    public UserService() {
         request = DataSource.getInstance().getRequest();
    }

    
    public void addUser(User t) {
        String url = Statics.BASE_URL+"/addUser?f_name="+ t.getFname()+ "&l_name="+t.getLname()+"&email="+t.getEmail()
                +"&pwd="+t.getPwd()+"&age="+t.getAge();
        request.setUrl(url);// Insertion de l'URL de notre demande de connexion
        request.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = request.getResponseCode() == 200; //Code HTTP 200 OK
                request.removeResponseListener(this); //Supprimer cet actionListener
                /* une fois que nous avons terminé de l'utiliser.
                La ConnectionRequest req est unique pour tous les appels de 
                n'importe quelle méthode du Service task, donc si on ne supprime
                pas l'ActionListener il sera enregistré et donc éxécuté même si 
                la réponse reçue correspond à une autre URL(get par exemple)*/
                /*String str = new String(request.getResponseData());
                System.out.println("data = " + str);*/
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(request);
        //return resultOK;
    }

    
    public ArrayList<User> parseUser (String jsonText){
        try {
            users=new ArrayList<>();
            JSONParser j = new JSONParser();// Instanciation d'un objet JSONParser permettant le parsing du résultat json

            /*
                On doit convertir notre réponse texte en CharArray à fin de
            permettre au JSONParser de la lire et la manipuler d'ou vient 
            l'utilité de new CharArrayReader(json.toCharArray())
            
            La méthode parse json retourne une MAP<String,Object> ou String est 
            la clé principale de notre résultat.
            Dans notre cas la clé principale n'est pas définie cela ne veux pas
            dire qu'elle est manquante mais plutôt gardée à la valeur par defaut
            qui est root.
            En fait c'est la clé de l'objet qui englobe la totalité des objets 
                    c'est la clé définissant le tableau de tâches.
            */
            Map<String,Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            
              /* Ici on récupère l'objet contenant notre liste dans une liste 
            d'objets json List<MAP<String,Object>> ou chaque Map est une tâche.               
            
            Le format Json impose que l'objet soit définit sous forme
            de clé valeur avec la valeur elle même peut être un objet Json.
            Pour cela on utilise la structure Map comme elle est la structure la
            plus adéquate en Java pour stocker des couples Key/Value.
            
            Pour le cas d'un tableau (Json Array) contenant plusieurs objets
            sa valeur est une liste d'objets Json, donc une liste de Map
            */
            List<Map<String,Object>> list = (List<Map<String,Object>>)tasksListJson.get("root");
            
            //Parcourir la liste des tâches Json
            for(Map<String,Object> obj : list){
                //Création des tâches et récupération de leurs données
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());
                t.setId((int)id);
                t.setAge((int)Float.parseFloat(obj.get("age").toString()));
                t.setFname(obj.get("f_name").toString());
                t.setLname(obj.get("l_name").toString());
                t.setEmail(obj.get("email").toString());
                t.setPwd(obj.get("pwd").toString());
                //Ajouter la tâche extraite de la réponse Json à la liste
                users.add(t);
            }
            
            
        } catch (IOException ex) {
            System.out.println("error related to" + ex.getMessage());
        }
         /*
            A ce niveau on a pu récupérer une liste des tâches à partir
        de la base de données à travers un service web
        
        */
        return users;
    }
    
    public ArrayList<User> getAllUser(){
        ArrayList<User> result = new ArrayList();
        String url = Statics.BASE_URL+"/afficheUser";
        request.setUrl(url);
        //request.setPost(false);
        request.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                /*users = parseUser(new String(request.getResponseData()));
                request.removeResponseListener(this);*/
                JSONParser j = new JSONParser();
                try {

            // Instanciation d'un objet JSONParser permettant le parsing du résultat json

            Map<String,Object> tasksListJson = j.parseJSON(new CharArrayReader(new String(request.getResponseData()).toCharArray()));
            List<Map<String,Object>> list = (List<Map<String,Object>>)tasksListJson.get("root");
          
            for(Map<String,Object> obj : list){
                //Création des tâches et récupération de leurs données
                User t = new User();
                float id = Float.parseFloat(obj.get("id").toString());

                String email= obj.get("email").toString();
                String nom= (String) obj.get("f_name");
                String prenom= obj.get("l_name").toString();
                String pwd= obj.get("pwd").toString();
                float age= Float.parseFloat(obj.get("age").toString());
                
                t.setId((int)id);
                t.setFname(nom);
                t.setLname(prenom);
                t.setEmail(email);
                t.setPwd(pwd);
                t.setAge((int)age);
                
                result.add(t);
            }
        } catch (IOException ex) {
            System.out.println("error related to" + ex.getMessage());
        }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(request);
        return result;
    }
    
    
    public User detailUser(int id, User user){
        String url = Statics.BASE_URL+"/detailUser?"+ id;
        request.setUrl(url);
        request.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                users = parseUser(new String(request.getResponseData()));
                request.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(request);
        return user;
    }
    
}
