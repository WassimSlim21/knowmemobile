/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import java.util.List;
import com.codename1.ui.events.ActionListener;
import entities.Match;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import utils.Statics;
/**
 *
 * @author wassi
 */
public class MatchService {
    private static MatchService instance = null;
    private ConnectionRequest req;
    private boolean resultOK;
    private ArrayList<Match> matchs;

    private MatchService() {
        this.matchs = new ArrayList<Match>();
        req = new ConnectionRequest();
    }
    public static MatchService getInstance() {
        if (instance == null) {
            instance = new MatchService();
        }
        return instance;
    }

    public ArrayList<Match> findAll(){
        String url = Statics.BASE_URL + "/api/match/list";
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                matchs = parseMatchs(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return matchs;
    }

       public ArrayList<Match> parseMatchs(String jsonText){
        try {
            matchs =new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String,Object> matchListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String,Object>> list = (List<Map<String,Object>>) matchListJson.get("root");


            for(Map<String,Object> obj : list){
                
                Match m = new Match();
                float id = Float.parseFloat(obj.get("id").toString());
                m.setId((int)id);
                m.setUser1(Integer.parseInt(obj.get("user1").toString()));
                m.setUser2(Integer.parseInt(obj.get("user2").toString()));
                m.setDate(obj.get("date").toString());
                //System.out.println("Matchs : " + obj.get("menu"));
                System.out.println("Matchs :"+ matchs);
                matchs.add(m);
            }
            return matchs;

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }


 /*   public boolean update(Room room) {
        String url = Statics.BASE_URL + "/api/room/update/" + room.getId() + "?name=" + room.getName() + "&description=" + room.getDescription() +"&location=" + room.getLocation() +"&menu=" + room.getMenu().getId();
        req.setUrl(url);
        req.setContentType("application/json");
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }

    public boolean delete(int id) {
        String url = Statics.BASE_URL + "/api/room/delete/"+id;
        req.setUrl(url);
        req.setHttpMethod("DELETE");
        req.setContentType("application/json");
        try {
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                    req.removeResponseListener(this);
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            return resultOK;
        } catch (Exception e) {
            return false;
        }

    }


    
    public boolean save(Room room) {
        String url = Statics.BASE_URL + "/api/room/add";
        req.setUrl(url);
        req.setPost(true);
        req.setContentType("application/json");
        try {
            System.out.println("hani fi wist lajout");
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("name", room.getName());
            hashMap.put("description", room.getDescription());
            hashMap.put("location", room.getLocation());
            hashMap.put("image", room.getImage());
            hashMap.put("menu", room.getMenu().getId());
            System.out.println(hashMap);
            req.setRequestBody(Result.fromContent(hashMap).toString());
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                    req.removeResponseListener(this);
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            return resultOK;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }*/
}
