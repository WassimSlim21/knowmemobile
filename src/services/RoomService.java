/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import static com.codename1.io.Log.e;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.processing.Result;
import com.codename1.ui.events.ActionListener;
import java.util.List;
import com.codename1.ui.events.ActionListener;
import entities.Event;
import entities.Menu;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import utils.DataSource;
import utils.Statics;
import java.util.Map;
import entities.Room;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.json.JSONObject;
import utils.Statics;
/**
 *
 * @author wassi
 */
public class RoomService {
    private static RoomService instance = null;
    private ConnectionRequest req;
    private boolean resultOK;
    private ArrayList<Room> rooms;

    private RoomService() {
        this.rooms = new ArrayList<Room>();
        req = new ConnectionRequest();
    }
    public static RoomService getInstance() {
        if (instance == null) {
            instance = new RoomService();
        }
        return instance;
    }

    public ArrayList<Room> findAll(){
        String url = Statics.BASE_URL + "/api/room/list";
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                rooms = parseRooms(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return rooms;
    }

    public ArrayList<Room> parseRooms(String jsonText){
        try {
            rooms =new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String,Object> roomListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String,Object>> list = (List<Map<String,Object>>)roomListJson.get("root");


            for(Map<String,Object> obj : list){
                
                Room r = new Room();
                float id = Float.parseFloat(obj.get("id").toString());
                r.setId((int)id);
                r.setName((obj.get("name").toString()));
                r.setDescription(obj.get("description").toString());
                r.setLocation(obj.get("location").toString());
                r.setImage(obj.get("image").toString());
   //JSONParser parser = new JSONParser();
   //JSONObject json = (JSONObject) parser.parse(obj.get("menu").toString());

                Menu menu = new Menu();
                
                //menu.setId(Integer.parseInt(obj.get("menu.id").toString()));
               // menu.setImg(obj.get("menu.tmg"));
                //r.setMenu(obj.get("menu"));
                System.out.println("Menu : " + obj.get("menu"));
                System.out.println("Room :"+ r);
                rooms.add(r);
            }
            return rooms;

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public boolean update(Room room) {
        String url = Statics.BASE_URL + "/api/room/update/" + room.getId() + "?name=" + room.getName() + "&description=" + room.getDescription() +"&location=" + room.getLocation() +"&menu=" + room.getMenu().getId();
        req.setUrl(url);
        req.setContentType("application/json");
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }

    public boolean delete(int id) {
        String url = Statics.BASE_URL + "/api/room/delete/"+id;
        req.setUrl(url);
        req.setHttpMethod("DELETE");
        req.setContentType("application/json");
        try {
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                    req.removeResponseListener(this);
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            return resultOK;
        } catch (Exception e) {
            return false;
        }

    }


    
    public boolean save(Room room) {
        String url = Statics.BASE_URL + "/api/room/add";
        req.setUrl(url);
        req.setPost(true);
        req.setContentType("application/json");
        try {
            System.out.println("hani fi wist lajout");
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("name", room.getName());
            hashMap.put("description", room.getDescription());
            hashMap.put("location", room.getLocation());
            hashMap.put("image", room.getImage());
            hashMap.put("menu", room.getMenu().getId());
            System.out.println(hashMap);
            req.setRequestBody(Result.fromContent(hashMap).toString());
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                    req.removeResponseListener(this);
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            return resultOK;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
